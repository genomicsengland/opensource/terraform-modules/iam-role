## v1.0.2 - 2020-04-14

- Change policy variables to be consistent and reflect their purpose
- Add IAM Permission Boundary support

## v1.0.1 - 2019-12-22

- Implement additional IAM role arguments (`description`, `force_detach_policies`, `max_session_duration`)
- Update documentation to include full implementation

## v1.0.0 - 2019-12-20

- Inital release
