# Terraform AWS IAM Role Module

Creates an IAM role with role and trust policy attachments

## Purpose

This module is designed to simplify and standardise the creation of AWS IAM Roles and the attachment of IAM Role Policies with Terraform

It _is not_ designed to tackle the complexities of creating IAM Role Policies.  You still need to create these and supply them as inputs to this module

## Role Policy Types

This module supports:
- Customer managed policies
- AWS managed policies
- Permissions boundaries

## Simple Implementation

The below snippet is a simple implementation for creating an IAM role with an associated Instance Profile

```hcl
module "iam_role" {
  source = "git::https://gitlab.com/genomicsengland/opensource/terraform-modules/iam-role?ref=<version_tag>"

  create              = var.create
  is_instance_profile = true
  name                = "MyAwesomeIAMRole"
  description         = "IAM Role to do something"

  trust_policy = concat(data.aws_iam_policy_document.MyTrustPolicy.*.json, [""])[0]

  customer_policies = [
    {
      name        = "MyAwesomeIAMRolePolicy"
      description = "A customer managed IAM Policy document"
      policy      = concat(data.aws_iam_policy_document.MyAwesomeIAMRolePolicy.*.json, [""])[0]
    }
  ]

  managed_policy_arns = [
    "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
  ]
}
```

The above implementation assumes you're using Terraform Data Sources to create the IAM role and trust policies

```hcl
data "aws_iam_policy_document" "MyTrustPolicy" {
  count = var.create ? 1 : 0

  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["something.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

data "aws_iam_policy_document" "MyAwesomeIAMRolePolicy" {
  count = var.create ? 1 : 0

  statement {
    effect    = "Allow"
    actions   = ["s3:ListBuckets"]
    resources = ["*"]
  }
}
```
